var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "36660",
        "ok": "16138",
        "ko": "20522"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "79",
        "ko": "0"
    },
    "maxResponseTime": {
        "total": "60644",
        "ok": "60644",
        "ko": "60006"
    },
    "meanResponseTime": {
        "total": "9882",
        "ok": "1703",
        "ko": "16313"
    },
    "standardDeviation": {
        "total": "12314",
        "ok": "2282",
        "ko": "13146"
    },
    "percentiles1": {
        "total": "1725",
        "ok": "949",
        "ko": "10718"
    },
    "percentiles2": {
        "total": "20020",
        "ok": "1645",
        "ko": "30018"
    },
    "percentiles3": {
        "total": "30282",
        "ok": "5884",
        "ko": "30423"
    },
    "percentiles4": {
        "total": "30588",
        "ok": "7284",
        "ko": "30680"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4384,
    "percentage": 12
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 5899,
    "percentage": 16
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 5855,
    "percentage": 16
},
    "group4": {
    "name": "failed",
    "count": 20522,
    "percentage": 56
},
    "meanNumberOfRequestsPerSecond": {
        "total": "407.333",
        "ok": "179.311",
        "ko": "228.022"
    }
},
contents: {
"req_count-e2942": {
        type: "REQUEST",
        name: "count",
path: "count",
pathFormatted: "req_count-e2942",
stats: {
    "name": "count",
    "numberOfRequests": {
        "total": "36660",
        "ok": "16138",
        "ko": "20522"
    },
    "minResponseTime": {
        "total": "0",
        "ok": "79",
        "ko": "0"
    },
    "maxResponseTime": {
        "total": "60644",
        "ok": "60644",
        "ko": "60006"
    },
    "meanResponseTime": {
        "total": "9882",
        "ok": "1703",
        "ko": "16313"
    },
    "standardDeviation": {
        "total": "12314",
        "ok": "2282",
        "ko": "13146"
    },
    "percentiles1": {
        "total": "1725",
        "ok": "949",
        "ko": "10718"
    },
    "percentiles2": {
        "total": "20020",
        "ok": "1646",
        "ko": "30018"
    },
    "percentiles3": {
        "total": "30282",
        "ok": "5887",
        "ko": "30423"
    },
    "percentiles4": {
        "total": "30588",
        "ok": "7284",
        "ko": "30680"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 4384,
    "percentage": 12
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 5899,
    "percentage": 16
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 5855,
    "percentage": 16
},
    "group4": {
    "name": "failed",
    "count": 20522,
    "percentage": 56
},
    "meanNumberOfRequestsPerSecond": {
        "total": "407.333",
        "ok": "179.311",
        "ko": "228.022"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
