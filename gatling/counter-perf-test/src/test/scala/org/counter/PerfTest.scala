package org.counter

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._

class PerfTest extends Simulation {

  val url: String = "http://actix-counter.herokuapp.com"

  val httpConfiguration = http.baseUrl(url).shareConnections

  val genericScenario = scenario("actix-counter").exec(
    http("count").get("/")).inject(
    constantUsersPerSec(1111) during(30 seconds),
  )
  setUp(genericScenario).protocols(httpConfiguration)
}
