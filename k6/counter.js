import http from 'k6/http';
import { check } from 'k6';

export const options = {
  scenarios: {
    counter: {
      executor: 'constant-arrival-rate',
      rate: 1222,
      duration: '30s',
      preAllocatedVUs: 100,
      maxVUs: 50000,
    },
  },
};


export default function () {
    const res = http.get("http://actix-counter.herokuapp.com/");
    check(res, {
	'is 200': (r) => r.status === 200,
	'body size > 0': (r) => r.body.length > 0,
  });
}