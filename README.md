## Explain solution: 
AUT is very performant rust application that counts incoming HTTP/S requests based on this [example](https://github.com/actix/examples/tree/master/basics/state) and available on this url: [actix-counter.herokuapp.com](http://actix-counter.herokuapp.com)       

k6 script consists of following elements:

 * `constant-arrival-rate` to use  [open model](https://k6.io/docs/using-k6/scenarios/arrival-rate/) 
 * `preAllocatedVUs: 100` - to mitigate water hammer effect of 1k RPS 
 * `maxVUs: 50000` - to have headroom of VUs in case system will reach [non-stationary state](https://www0.gsb.columbia.edu/mygsb/faculty/research/pubfiles/4376/green_kolesar_nonstationarity.pdf)
 * Duration 30 seconds to be able to see some effects caused by connection/request timeouts
 * HTTP endpoint was used instead of HTTPS to eliminate TLS-related overhead on both client and server
 * Checks were put to ensure that response is present and to make it more apples-to-apples comparison with gatling script

To execute test install k6 and run `k6 run k6/counter.js`

Local execution(127.0.0.1 to eliminate DNS resolution) without RPS limitation gives following results so we can consider 60k as a baseline(65k if don't use response checks and body parsing):
 
             ✓ is 200
             ✓ body size > 0
        
             checks.........................: 100.00% ✓ 3730156      ✗ 0
             data_received..................: 186 MB  6.2 MB/s
             data_sent......................: 149 MB  5.0 MB/s
             http_req_blocked...............: avg=2.5µs    min=0s      med=2µs      max=40.39ms  p(90)=2µs      p(95)=3µs
             http_req_connecting............: avg=10ns     min=0s      med=0s       max=904µs    p(90)=0s       p(95)=0s
             http_req_duration..............: avg=353.17µs min=54µs    med=299µs    max=94.24ms  p(90)=526µs    p(95)=677µs
               { expected_response:true }...: avg=353.17µs min=54µs    med=299µs    max=94.24ms  p(90)=526µs    p(95)=677µs
             http_req_failed................: 0.00%   ✓ 0            ✗ 1865078
             http_req_receiving.............: avg=28.51µs  min=5µs     med=17µs     max=92.73ms  p(90)=22µs     p(95)=25µs
             http_req_sending...............: avg=11.74µs  min=3µs     med=8µs      max=68.51ms  p(90)=9µs      p(95)=11µs
             http_req_tls_handshaking.......: avg=0s       min=0s      med=0s       max=0s       p(90)=0s       p(95)=0s
             http_req_waiting...............: avg=312.92µs min=36µs    med=271µs    max=47.58ms  p(90)=488µs    p(95)=623µs
             http_reqs......................: 1865078 62166.386291/s
             iteration_duration.............: avg=474.22µs min=91.17µs med=371.84µs max=115.59ms p(90)=650.11µs p(95)=887.93µs
             iterations.....................: 1865078 62166.386291/s
             vus............................: 30      min=30         max=30
             vus_max........................: 30      min=30         max=30
             
Every check is passed, no dropped iterations, p(95) is 623µs which is pretty fast :)
AVG number of requests (Little's law) = 23 (62k rps X 371.84µs)            

## Analyse HTTP response

Remote execution with open model and RPS lock (**1111 iterations/s**) has totally different picture though

     ✓ is 200
     ✓ body size > 0

     checks.........................: 100.00% ✓ 66034       ✗ 0    
     data_received..................: 5.1 MB  167 kB/s
     data_sent......................: 3.1 MB  101 kB/s
     dropped_iterations.............: 314     10.297235/s
     http_req_blocked...............: avg=3.9ms    min=1µs      med=2µs      max=401.07ms p(90)=3µs      p(95)=4µs     
     http_req_connecting............: avg=3.8ms    min=0s       med=0s       max=401ms    p(90)=0s       p(95)=0s      
     http_req_duration..............: avg=235.32ms min=92.29ms  med=234.12ms max=434.98ms p(90)=275.6ms  p(95)=281.05ms
       { expected_response:true }...: avg=235.32ms min=92.29ms  med=234.12ms max=434.98ms p(90)=275.6ms  p(95)=281.05ms
     http_req_failed................: 0.00%   ✓ 0           ✗ 33017
     http_req_receiving.............: avg=24.69µs  min=11µs     med=22µs     max=679µs    p(90)=34µs     p(95)=43µs    
     http_req_sending...............: avg=10.19µs  min=4µs      med=9µs      max=587µs    p(90)=12µs     p(95)=17µs    
     http_req_tls_handshaking.......: avg=0s       min=0s       med=0s       max=0s       p(90)=0s       p(95)=0s      
     http_req_waiting...............: avg=235.29ms min=91.67ms  med=234.08ms max=434.87ms p(90)=275.57ms p(95)=281.01ms
     http_reqs......................: 33017   1082.751009/s
     iteration_duration.............: avg=239.29ms min=118.78ms med=234.95ms max=680.39ms p(90)=277.14ms p(95)=283.48ms
     iterations.....................: 33017   1082.751009/s
     vus............................: 409     min=238       max=409
     vus_max........................: 409     min=238       max=409
     
Every check is passed, target RPS slightly lower than target(1111)  BUT there were **314 dropped** iterations which is 0.001% of all requests. 
Maybe it's not that critical but why k6 dropping requests?

AVG number of requests in the system = 254 

Increasing request rate to **1222 iterations/s** makes things even more interesting

     ✗ is 200
      ↳  87% — ✓ 27839 / ✗ 3986
     ✗ body size > 0
      ↳  87% — ✓ 27839 / ✗ 3986

     checks.........................: 87.47% ✓ 55678      ✗ 7972  
     data_received..................: 4.3 MB 71 kB/s
     data_sent......................: 2.6 MB 43 kB/s
    !dropped_iterations.............: 4835   80.578038/s <======== 15% of dropped/not sent requests! dropped_iterations	Counter	Introduced in k6 v0.27.0, the number of iterations that could not be started due to lack of VUs (for the arrival-rate executors) or lack of time (due to expired maxDuration in the iteration-based executors).
     http_req_blocked...............: avg=10.03ms  min=0s       med=2µs      max=29.7s  p(90)=3µs   p(95)=5µs  
     http_req_connecting............: avg=9.93ms   min=0s       med=0s       max=29.7s  p(90)=0s    p(95)=0s   
     http_req_duration..............: avg=741.92ms min=0s       med=750.87ms max=1.58s  p(90)=1.32s p(95)=1.41s
       { expected_response:true }...: avg=848.18ms min=83.06ms  med=815.46ms max=1.58s  p(90)=1.33s p(95)=1.43s
     http_req_failed................: 12.52% ✓ 3987       ✗ 27839 
     http_req_receiving.............: avg=23.33µs  min=0s       med=22µs     max=642µs  p(90)=37µs  p(95)=49µs 
     http_req_sending...............: avg=9.6µs    min=0s       med=8µs      max=2.47ms p(90)=13µs  p(95)=20µs 
     http_req_tls_handshaking.......: avg=0s       min=0s       med=0s       max=0s     p(90)=0s    p(95)=0s   
     http_req_waiting...............: avg=741.89ms min=0s       med=750.84ms max=1.58s  p(90)=1.32s p(95)=1.41s
     http_reqs......................: 31826  530.398477/s
     iteration_duration.............: avg=4.5s     min=167.63ms med=865.82ms max=30.03s p(90)=30s   p(95)=30s  
     iterations.....................: 31825  530.381812/s
     vus............................: 4856   min=527      max=4856
     vus_max........................: 4856   min=527      max=4856
     
     
    WARN[0060] Request Failed                                error="Get \"http://actix-counter.herokuapp.com/\": dial: i/o timeout"
    ERRO[0060] TypeError: Cannot read property 'length' of undefined
    running at bodySize0 (file:///k6/counter.js:22:52(3))
    contactsat go.k6.io/k6/js/common.Bind.func1 (native)
            at file:///k6/k6_src/ttest/k6/counter.js:20:22(16)  executor=constant-arrival-rate scenario=contacts source=stacktrace

Checks and requests failing, dropped_iterations = 4835 which is **15% of overall req count**, RPS dropped x2 to 530 RPS and VUs grown to 4.8k so it's obviously **[non-stationary state](http://perfdynamics.blogspot.com/2015/07/hockey-elbow-and-other-response-time.html)**

AVG number of requests in the system = 458


Let's profile k6 to check what's going on there during test execution. To do that just import "net/http/pprof" in main.go and recompile k6

![k6 CPU profile](k6_profiling.png)

Increasing VU count increase k6 overhead (caused by [go scheduler](https://www.binwang.me/2014-09-01-Notes-On-Go-Scheduler.html) and GC) so this could be the main reason behind **dropped_iterations**

Let's check how scalable k6 VUs

RPS | VUs
------------ | -------------
9 | 1
469 | 50
863 | 100
1009 | 200
999 | 300
1003 | 400
1063 | 500
1050 | 600
1085 | 700
1060 | 800
![k6 scalability](k6_scaling.png)

Suspiciously looks like some bottleneck 

Let's use Gatling to do the same thing - load test with 1111 RPS and then 1222 RPS to check if there's bottleneck involved

To execute gatling test please install maven and execute  `mvn gatling:test` in `gatling/counter-perf-test` folder


#### [1111 RPS report](gatling/counter-perf-test/report/1111/)

        ------------- Global Information --------------------------------------------------------
        > request count                                      33330 (OK=33330  KO=0     )
        > min response time                                     76 (OK=76     KO=-     )
        > max response time                                    410 (OK=410    KO=-     )
        > mean response time                                   114 (OK=114    KO=-     )
        > std deviation                                         46 (OK=46     KO=-     )
        > response time 50th percentile                         90 (OK=90     KO=-     )
        > response time 75th percentile                        130 (OK=130    KO=-     )
        > response time 95th percentile                        207 (OK=207    KO=-     )
        > response time 99th percentile                        230 (OK=230    KO=-     )
        > mean requests/sec                                1075.161 (OK=1075.161 KO=-     )
        ---- Response Time Distribution ------------------------------------------------
        > t < 800 ms                                         33330 (100%)
        > 800 ms < t < 1200 ms                                   0 (  0%)
        > t > 1200 ms                                            0 (  0%)
        > failed                                                 0 (  0%)
        ================================================================================

All checks passed, no requests drops but RPS is slightly lower than requested. Increasing to 1222 RPS


#### [1222 RPS report](gatling/counter-perf-test/report/1222/)

        ------------- Global Information --------------------------------------------------------
        > request count                                      36660 (OK=16138  KO=20522 )
        > min response time                                      0 (OK=79     KO=0     )
        > max response time                                  60644 (OK=60644  KO=60006 )
        > mean response time                                  9882 (OK=1703   KO=16313 )
        > std deviation                                      12314 (OK=2282   KO=13146 )
        > response time 50th percentile                       1725 (OK=949    KO=10718 )
        > response time 75th percentile                      20020 (OK=1645   KO=30018 )
        > response time 95th percentile                      30282 (OK=5884   KO=30423 )
        > response time 99th percentile                      30588 (OK=7284   KO=30680 )
        > mean requests/sec                                407.333 (OK=179.311 KO=228.022)
        ---- Response Time Distribution ------------------------------------------------
        > t < 800 ms                                          4384 ( 12%)
        > 800 ms < t < 1200 ms                                5899 ( 16%)
        > t > 1200 ms                                         5855 ( 16%)
        > failed                                             20522 ( 56%)
        ---- Errors --------------------------------------------------------------------
        > j.n.SocketException: Too many open files                        11223 (54,69%)
        > i.n.c.ConnectTimeoutException: connection timed out: actix-cou   3077 (14,99%)
        nter.herokuapp.com/54.73.53.134:80
        > i.n.c.ConnectTimeoutException: connection timed out: actix-cou   3058 (14,90%)
        nter.herokuapp.com/46.137.15.86:80
        > i.n.c.ConnectTimeoutException: connection timed out: actix-cou   3057 (14,90%)
        nter.herokuapp.com/54.220.192.176:80
        > i.g.h.c.i.RequestTimeoutException: Request timeout to actix-co     38 ( 0,19%)
        unter.herokuapp.com/54.220.192.176:80 after 60000 ms
        > i.g.h.c.i.RequestTimeoutException: Request timeout to actix-co     35 ( 0,17%)
        unter.herokuapp.com/46.137.15.86:80 after 60000 ms
        > i.g.h.c.i.RequestTimeoutException: Request timeout to actix-co     24 ( 0,12%)
        unter.herokuapp.com/54.73.53.134:80 after 60000 ms
        > j.n.UnknownHostException: actix-counter.herokuapp.com               9 ( 0,04%)
        > j.n.UnknownHostException: actix-counter.herokuapp.com: nodenam      1 ( 0,00%)
        e nor servname provided, or not known
        ================================================================================


Ok, so it looks like OS with load generator hit max open files value. Despite I've tried increasing the number and  
`launchctl limit maxfiles
maxfiles    1000000        200000000`

`ulimit -n` gives 200000 which is causing this issue

**Please note that gatling does not drop any requests in contrary to what k6 does!**
             
             
## Did the load test have an impact on web application response time?
 Yes! Slower response times caused non-stationary state which resulted in bigger open connection count and this caused "Too Many open files" issue and dropped iterations in k6
 
 k6 really poorly handles load testing in non-stationary states dropping requests!
 
             
             
## What is the optimal application response time for modern web applications?
 Depends on a use case. For microservices it's good practice to have latency <100ms due to requests chaining
 
 For web sites it's ok to have response time at ±1 second
 
 Google's [best practices](https://www.thinkwithgoogle.com/marketing-strategies/app-and-mobile/mobile-page-speed-new-industry-benchmarks/) is to have page ready in <3 seconds and time to first byte < 1.3s 
 
 
## How would you define acceptable load for web applications?
 "Acceptable" and "load" are pretty tricky terms in context of performance engineering.
 Let's say that "acceptable" refers to requirements so we need to have in some form (e.g. SLO/SLA) and "load" refers to requests arrival rate 
 
 For example: application receives 1 request to calculate monthly report and spend next 2 hours to calculate it
                                         Latency for this operation is 2 hours which gives us ±0,00014 rps which is rediciluosly low number 
                                         But spending 2 hours for a monthly report sounds pretty reasonable :)
                       
 So in general acceptable load is the load that: 
 
  - do not put system in non-stationary state
  - able to satisfy SLOs/SLAs
 
                            
 As soon as system is stable and latency satisfies agreed level - everything is fine.
 But traffic spikes, 3d parties and other bottlenecks makes these two points VERY tricky             